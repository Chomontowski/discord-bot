package pl.foxysoft.discord.bot.discord.config;

import lombok.SneakyThrows;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.hooks.AnnotatedEventManager;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class JdaConfig {
    private static final String JDA_BOT_TOKEN = "JDA_BOT_TOKEN";
    private static final String TOKEN = System.getenv().get(JDA_BOT_TOKEN);

    private JDA jda;

    @SneakyThrows
    @PostConstruct
    public void createJda() {
        jda = JDABuilder.createLight(TOKEN,
                GatewayIntent.GUILD_MESSAGES,
                GatewayIntent.GUILD_MESSAGE_REACTIONS)
                .setEventManager(new AnnotatedEventManager())
                .build();

    }

    @Bean
    public JDA jda() {
        return jda;
    }

}
