package pl.foxysoft.discord.bot.discord.listener;

import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;
import pl.foxysoft.discord.bot.message.MessageSender;
import pl.foxysoft.discord.exception.SessionException;
import pl.foxysoft.discord.service.SessionService;

@Component
@RequiredArgsConstructor
public class ActivateSessionListener implements DiscordListener {
    private static final String SHORT_COMMAND = "-a";
    private static final String LONG_COMMAND = "-activate";
    private static final String HELP = "Activates session under current category";

    private final SessionService sessionService;
    private final MessageSender messageSender;

    @Override
    public void execute(MessageReceivedEvent event) {
        try {
            if (event.getTextChannel().getParent() == null) {
                throw new SessionException("Parent category doesn't exist");
            }
            Long categoryId = event.getTextChannel().getParent().getIdLong();
            User author = event.getAuthor();
            sessionService.activateSession(categoryId, author);
            messageSender.sendChannelMessage(event.getTextChannel(), "Session has been activated.");
        } catch (Exception e) {
            messageSender.sendChannelMessage(event.getTextChannel(), e.getMessage());
        }
    }

    @Override
    public String getShortCommand() {
        return SHORT_COMMAND;
    }

    @Override
    public String getLongCommand() {
        return LONG_COMMAND;
    }

    @Override
    public String getHelpInfo() {
        return HELP;
    }

    @Override
    public String getDetailedHelpInfo() {
        return null;
    }
}
