package pl.foxysoft.discord.bot.discord.listener;


import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;
import pl.foxysoft.discord.bot.message.MessageSender;
import pl.foxysoft.discord.exception.SessionException;
import pl.foxysoft.discord.persistence.model.Character;
import pl.foxysoft.discord.persistence.model.Session;
import pl.foxysoft.discord.service.SessionService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;


@Component
@RequiredArgsConstructor
public class AddCharacterToSessionListener implements DiscordListener {
    private static final String SHORT_COMMAND = "-ac";
    private static final String LONG_COMMAND = "-addCharacter";
    private static final String HELP = "Add character to existing session";

    private final MessageSender messageSender;
    private final SessionService sessionService;

    @Override
    @Transactional
    public void execute(MessageReceivedEvent event) {
        List<Member> members = event.getMessage().getMentionedMembers();

        TextChannel textChannel = event.getTextChannel();

        try {
            if (textChannel.getParent() == null) {
                throw new SessionException("Parent category doesn't exist");
            }
            sessionService.checkSessionOwnership(event.getAuthor(), textChannel.getParent().getIdLong());
            if (members.isEmpty()) {
                messageSender.sendChannelMessage(textChannel, event.getAuthor().getAsMention() + " You can't add 0 characters! Mention someone!");
            } else {
                addCharactersToSession(members, textChannel);
            }
        } catch (Exception e) {
            messageSender.sendChannelMessage(textChannel, e.getMessage());
        }
    }

    private void addCharactersToSession(List<Member> members, TextChannel textChannel) throws SessionException {
        if (textChannel.getParent() == null) {
            throw new SessionException("Parent category doesn't exist");
        }
        Session session = sessionService.findSessionByDiscordCategoryId(textChannel.getParent().getIdLong());
        List<Long> characters = session.getCharacters().stream().map(Character::getDiscordId).collect(Collectors.toList());
        List<Member> usersToAdd = members.stream().filter(member -> !characters.contains(member.getIdLong())).collect(Collectors.toList());
        sessionService.addCharactersToSession(usersToAdd, textChannel.getParent().getIdLong(), textChannel.getGuild().getId());
    }

    @Override
    public String getShortCommand() {
        return SHORT_COMMAND;
    }

    @Override
    public String getLongCommand() {
        return LONG_COMMAND;
    }

    @Override
    public String getHelpInfo() {
        return HELP;
    }

    @Override
    public String getDetailedHelpInfo() {
        return null;
    }
}
