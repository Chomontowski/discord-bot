package pl.foxysoft.discord.bot.discord.listener;

import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;


@Component
@AllArgsConstructor
public class CharacterDeletionListener implements DiscordListener {
    private static final String SHORT_COMMAND = "-dc";
    private static final String LONG_COMMAND = "-deleteCharacter";
    private static final String HELP = "Deletes character from session";

    @Override
    public void execute(MessageReceivedEvent event) {

    }

    @Override
    public String getShortCommand() {
        return null;
    }

    @Override
    public String getLongCommand() {
        return null;
    }

    @Override
    public String getHelpInfo() {
        return null;
    }

    @Override
    public String getDetailedHelpInfo() {
        return null;
    }
}
