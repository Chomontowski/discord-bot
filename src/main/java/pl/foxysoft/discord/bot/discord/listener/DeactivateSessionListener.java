package pl.foxysoft.discord.bot.discord.listener;

import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;
import pl.foxysoft.discord.bot.message.MessageSender;
import pl.foxysoft.discord.service.SessionService;

@Component
@RequiredArgsConstructor
public class DeactivateSessionListener implements DiscordListener {
    private static final String SHORT_COMMAND = "-d";
    private static final String LONG_COMMAND = "-deactivate";
    private static final String HELP = "Deactivates session under current category";


    private final SessionService sessionService;
    private final MessageSender messageSender;
    @Override
    public void execute(MessageReceivedEvent event) {
        if (event.getTextChannel().getParent() != null) {
            messageSender.sendChannelMessage(event.getTextChannel(), "Parent category doesn't exist");
            return;
        }
        Long categoryId = event.getTextChannel().getParent().getIdLong();
        User author = event.getAuthor();
        try {
            sessionService.deactivateSession(categoryId, author);
            messageSender.sendChannelMessage(event.getTextChannel(), "Session has been deactivated. You can't change anything right now here!");
        } catch (Exception e) {
            messageSender.sendChannelMessage(event.getTextChannel(), e.getMessage());
        }
    }

    @Override
    public String getShortCommand() {
        return SHORT_COMMAND;
    }

    @Override
    public String getLongCommand() {
        return LONG_COMMAND;
    }

    @Override
    public String getHelpInfo() {
        return HELP;
    }

    @Override
    public String getDetailedHelpInfo() {
        return null;
    }
}
