package pl.foxysoft.discord.bot.discord.listener;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public interface DiscordListener {
    void execute(MessageReceivedEvent event);

    String getShortCommand();

    String getLongCommand();

    String getHelpInfo();

    String getDetailedHelpInfo();
}
