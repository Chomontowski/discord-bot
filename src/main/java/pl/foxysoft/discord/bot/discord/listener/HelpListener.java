package pl.foxysoft.discord.bot.discord.listener;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;
import pl.foxysoft.discord.bot.message.MessageSender;

import java.util.ArrayList;
import java.util.List;

@Component
public class HelpListener implements DiscordListener {
    private static final String HELP_SHORT_COMMAND = "h";
    private static final String HELP_LONG_COMMAND = "help";
    private static final String HELP = "Shows this message.";

    private final MessageSender messageSender;

    private final List<DiscordListener> listeners = new ArrayList<>();

    HelpListener(MessageSender messageSender,
                 RollListener rollListener,
                 SessionCreationListener sessionCreationListener,
                 SessionDeletionListener sessionDeletionListener,
                 ActivateSessionListener activateSessionListener,
                 DeactivateSessionListener deactivateSessionListener,
                 AddCharacterToSessionListener addCharacterToSessionListener) {
        this.messageSender = messageSender;
        listeners.add(rollListener);
        listeners.add(sessionCreationListener);
        listeners.add(sessionDeletionListener);
        listeners.add(activateSessionListener);
        listeners.add(deactivateSessionListener);
        listeners.add(addCharacterToSessionListener);
    }

    @Override
    public void execute(MessageReceivedEvent event) {
        messageSender.sendChannelMessage(event.getChannel(), prepareHelpInfo());
    }

    @Override
    public String getShortCommand() {
        return HELP_SHORT_COMMAND;
    }

    @Override
    public String getLongCommand() {
        return HELP_LONG_COMMAND;
    }

    @Override
    public String getHelpInfo() {
        return HELP;
    }

    @Override
    public String getDetailedHelpInfo() {
        return null;
    }

    private String prepareHelpInfo() {
        StringBuilder builder = new StringBuilder("```");

        builder.append("help - ").append(HELP).append("\n");

        listeners.forEach(listener -> {
            if (!listener.getShortCommand().equals(listener.getLongCommand())) {
                builder.append(listener.getShortCommand());
                builder.append(" / ");
            }
            builder.append(listener.getLongCommand());

            builder.append(" - ");
            builder.append(listener.getHelpInfo());
            builder.append("\n");
        });

        builder.append("```");
        return builder.toString();
    }
}
