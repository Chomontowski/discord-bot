package pl.foxysoft.discord.bot.discord.listener;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.SubscribeEvent;
import org.springframework.stereotype.Component;
import pl.foxysoft.discord.exception.BotException;
import pl.foxysoft.discord.exception.CouldNotFindListenerException;
import pl.foxysoft.discord.bot.discord.strategy.ListenerStrategy;
import pl.foxysoft.discord.bot.message.MessageSender;

import javax.annotation.Nonnull;

import static pl.foxysoft.discord.bot.message.MessageConstants.LISTENER_NOT_FOUND;

@Slf4j
@Component
@RequiredArgsConstructor
public class MainListener {
    private final ListenerStrategy listenerStrategy;
    private final MessageSender messageSender;

    @SubscribeEvent
    public void handleMessage(@Nonnull MessageReceivedEvent event) {
        try {
            isBot(event);
            String splittedCommand = event.getMessage().getContentRaw().split("/w")[1].trim();
            log.debug("Not a bot. Trying to get listener,");
            DiscordListener discordListener = listenerStrategy.getProperListener(splittedCommand);
            log.debug("Found listener!");
            discordListener.execute(event);
        } catch (BotException e) {
            log.debug("Message from bot. Terminating execution");
        } catch (CouldNotFindListenerException e) {
            log.error("Couldn't find proper listener.");
            messageSender.sendChannelMessage(event, LISTENER_NOT_FOUND);
        }
    }

    private void isBot(MessageReceivedEvent event) throws BotException {
        if (event.getAuthor().isBot()) {
            throw new BotException();
        }
    }
}
