package pl.foxysoft.discord.bot.discord.listener;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import pl.foxysoft.discord.bot.message.MessageSender;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

@Slf4j
@Component
@AllArgsConstructor
public class RollListener implements DiscordListener {
    private static final List<String> FUNNY_TEXTS = new ArrayList<>();
    private static final Random RANDOM = new Random();
    private static final String YOUR_ROLLS = "Your rolls: ";
    private static final String LINE_SEPARATOR = System.lineSeparator();
    private static final String HIGHEST_ROLL = "Highest roll: ";
    private static final String ROLL_SHORT_COMMAND = "r";
    private static final String ROLL_LONG_COMMAND = "roll";
    private static final String FEWER_DICE_AT_ONCE = "Please roll fewer dice at once!";
    private static final String HELP = "Rolls a dice(s). For example: \"r\" - rolls 1 dice. \"r 2\" rolls 2 dices. etc";
    private final MessageSender messageSender;

    static {
        FUNNY_TEXTS.add("Praca, Praca");
        FUNNY_TEXTS.add("Fallout 3 to zuooooo!! Dlatego rzucam, aby znikło!");
        FUNNY_TEXTS.add("Dobra, już dobra. Daj mi chociaż kawę dopić");
        FUNNY_TEXTS.add("Nie szturchaj mnie, @#%$@#, bo mi się nie chce");
    }

    @Override
    public void execute(MessageReceivedEvent event) {
        String message = event.getMessage().getContentRaw();

        List<Integer> rolls = makeRolls(getNumberOfDices(message));
        StringBuilder builder = createTextMessage(rolls);

        if (builder.length() > 2000) {
            messageSender.sendChannelMessageWithAuthorMention(event, FEWER_DICE_AT_ONCE);
        } else {
            messageSender.sendChannelMessageWithAuthorMention(event, builder.toString());
        }
    }

    @NotNull
    Integer getNumberOfDices(String message) {
        String[] splittedCommand = message.split("/w")[1].trim().split(" ");

        if (splittedCommand.length > 1) {
            String dices = message.split("/w")[1].trim().split(" ")[1];
            return Integer.valueOf(dices);
        }
        return 1;
    }

    List<Integer> makeRolls(Integer dices) {
        List<Integer> rolls = new LinkedList<>();
        IntStream.range(0, dices).forEach(dice -> {
            Integer roll = RANDOM.nextInt(10) + 1;
            rolls.add(roll);
        });
        return rolls;
    }

    @NotNull
    StringBuilder createTextMessage(List<Integer> rolls) {
        StringBuilder builder = new StringBuilder(getARandomFunnyText());
        builder.append(createRollsText(rolls));
        return builder;
    }

    String createRollsText(List<Integer> rolls) {
        StringBuilder builder = new StringBuilder(YOUR_ROLLS);
        builder.append(LINE_SEPARATOR);
        builder.append("(");
        IntStream.range(0, rolls.size()).forEach(counter -> {
            builder.append(rolls.get(counter));
            if (counter == (rolls.size() - 1)) {
                builder.append(")");
            } else {
                builder.append(" + ");
            }
        });

        builder.append(LINE_SEPARATOR);
        builder.append(HIGHEST_ROLL).append(rolls.stream().max(Integer::compare).orElseThrow());
        return builder.toString();
    }

    String getARandomFunnyText() {
        return FUNNY_TEXTS.get(RANDOM.nextInt(FUNNY_TEXTS.size()));
    }

    @Override
    public String getShortCommand() {
        return ROLL_SHORT_COMMAND;
    }

    @Override
    public String getLongCommand() {
        return ROLL_LONG_COMMAND;
    }

    @Override
    public String getHelpInfo() {
        return HELP;
    }

    @Override
    public String getDetailedHelpInfo() {
        return null;
    }
}
