package pl.foxysoft.discord.bot.discord.listener;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.requests.restaction.RoleAction;
import org.springframework.stereotype.Component;
import pl.foxysoft.discord.bot.message.MessageSender;
import pl.foxysoft.discord.persistence.model.Category;
import pl.foxysoft.discord.persistence.model.Character;
import pl.foxysoft.discord.persistence.model.Session;
import pl.foxysoft.discord.service.CategoryService;
import pl.foxysoft.discord.service.CharacterService;
import pl.foxysoft.discord.service.SessionService;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static pl.foxysoft.discord.bot.message.MessageConstants.CHARACTER_CREATED;
import static pl.foxysoft.discord.bot.message.MessageConstants.SESSION_CREATED_MESSAGE;

@Slf4j
@Component
@RequiredArgsConstructor
public class SessionCreationListener implements DiscordListener {
    private static final String CREATE_SESSION_LONG_COMMAND = "-createSession";
    private static final String CREATE_SESSION_SHORT_COMMAND = "-cs";
    private static final Pattern pattern = Pattern.compile("-name [a-zA-Z1-9 ]+");
    private static final String HELP = "Creates a session (with category and roles) based on provided users (as mentions) and name (-name addition)";

    private final SessionService sessionService;
    private final CharacterService characterService;
    private final MessageSender messageSender;
    private final CategoryService categoryService;

    @Override
    public void execute(MessageReceivedEvent event) {
        Member owner = event.getMember();
        if (owner == null) {
            messageSender.sendChannelMessage(event, "Owner doesn't exist!");
            return;
        }
        String sessionName = retrieveSessionName(event);
        Optional<Session> maybeSession = sessionService.sessionCheck(event.getGuild(), sessionName);

        if (maybeSession.isPresent()) {
            messageSender.sendChannelMessage(event, "Session with that name already exists! Please take another name!");
        } else {
            Role role = createSessionRole(event.getGuild(), event.getMessage().getMentionedMembers(), sessionName);
            Category category = categoryService.createCategory(event.getGuild(), role, sessionName);
            Session session = sessionService.createSession(owner, event.getGuild().getId(), category, sessionName, role.getIdLong());

            messageSender.sendPrivateMessage(event, SESSION_CREATED_MESSAGE);
            List<Character> characters = characterService.createCharacters(event.getMessage().getMentionedMembers(), event.getGuild().getId());
            session.setCharacters(characters);
            sessionService.saveSession(session);
            sendMessageToMentionedUsers(event.getAuthor().getName(), characters);
        }
    }

    @SneakyThrows
    private Role createSessionRole(Guild guild, List<Member> mentionedUsers, String sessionName) {

        RoleAction roleAction = guild.createRole();

        Role role = roleAction.setName(sessionName).complete(true);

        mentionedUsers.forEach(member -> guild.addRoleToMember(member, role).queue());

        return role;
    }

    private String retrieveSessionName(MessageReceivedEvent event) {
        Matcher matcher = pattern.matcher(event.getMessage().getContentRaw());
        String name = "";
        if(matcher.find()) {
            name = matcher.group(0);
        }

        return name.split("-name")[1];
    }

    private void sendMessageToMentionedUsers(String authorName, List<Character> characters) {
        characters.forEach(character -> {
            val discordId = character.getDiscordId();
            messageSender.sendPrivateMessage(discordId, String.format(CHARACTER_CREATED, authorName, character.getId()));
        });
    }

    @Override
    public String getShortCommand() {
        return CREATE_SESSION_SHORT_COMMAND;
    }

    @Override
    public String getLongCommand() {
        return CREATE_SESSION_LONG_COMMAND;
    }

    @Override
    public String getHelpInfo() {
        return HELP;
    }

    @Override
    public String getDetailedHelpInfo() {
        return null;
    }
}
