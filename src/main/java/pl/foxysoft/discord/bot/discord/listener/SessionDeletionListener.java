package pl.foxysoft.discord.bot.discord.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;
import pl.foxysoft.discord.bot.message.MessageSender;

@Slf4j
@Component
@RequiredArgsConstructor
public class SessionDeletionListener implements DiscordListener {
    private static final String DELETE_SESSION_COMMAND = "-deleteSession";
    private static final String DELETE_HELP = "deletes the session that is rolling under specific category.";
    private final MessageSender messageSender;


    @Override
    public void execute(MessageReceivedEvent event) {
        messageSender.sendDeletionMessageWithReactions(event.getChannel(), null);
    }

    @Override
    public String getShortCommand() {
        return DELETE_SESSION_COMMAND;
    }

    @Override
    public String getLongCommand() {
        return DELETE_SESSION_COMMAND;
    }

    @Override
    public String getHelpInfo() {
        return DELETE_HELP;
    }

    @Override
    public String getDetailedHelpInfo() {
        return null;
    }
}
