package pl.foxysoft.discord.bot.discord.listener;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.SubscribeEvent;
import org.springframework.stereotype.Component;
import pl.foxysoft.discord.bot.discord.service.SessionDeletionService;
import pl.foxysoft.discord.exception.BotException;

import javax.annotation.Nonnull;

@Slf4j
@Component
@AllArgsConstructor
public class SessionDeletionReactionListener {
    private final SessionDeletionService sessionDeletionService;

    @SubscribeEvent
    public void onMessageReactionAdd(@Nonnull MessageReactionAddEvent event) {
//        event.getMessageId()

        try {
            isBot(event);
            if (event.getReaction().getReactionEmote().getAsCodepoints().equals("U+1f1fe")) {
                sessionDeletionService.deleteSessionPermanently(event);
            }
            else {
                event.getTextChannel().deleteMessageById(event.getMessageId());
            }
        } catch (BotException e) {
            log.debug("Reaction from bot. Terminating execution");
        }
    }

    private void isBot(MessageReactionAddEvent event) throws BotException {
        if (event.getMember().getUser().isBot()) {
            throw new BotException();
        }
    }
}
