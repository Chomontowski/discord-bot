package pl.foxysoft.discord.bot.discord.listener;

import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.springframework.stereotype.Component;
import pl.foxysoft.discord.bot.message.MessageSender;

/**
 *
 *  This is a test listener, that is connected with listener strategy.
 *  Only for testing purposes.
 *
 *  /w -test (for now ofc)
 *
 */


@Component
@RequiredArgsConstructor
public class TestListener implements DiscordListener {
    private static final String TEST_SHORT_COMMAND = "-test";

    private final MessageSender messageSender;

    @Override
    public void execute(MessageReceivedEvent event) {
//        messageSender.sendDeletionMessageWithReactions(event.getTextChannel());
    }

    @Override
    public String getShortCommand() {
        return TEST_SHORT_COMMAND;
    }

    @Override
    public String getLongCommand() {
        return TEST_SHORT_COMMAND;
    }

    @Override
    public String getHelpInfo() {
        return null;
    }

    @Override
    public String getDetailedHelpInfo() {
        return null;
    }
}
