package pl.foxysoft.discord.bot.discord.service;

import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import org.springframework.stereotype.Service;
import pl.foxysoft.discord.persistence.model.Session;
import pl.foxysoft.discord.service.CategoryService;
import pl.foxysoft.discord.service.CharacterService;
import pl.foxysoft.discord.service.SessionService;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class SessionDeletionService {
    private final SessionService sessionService;
    private final CategoryService categoryService;
    private final CharacterService characterService;

    @Transactional
    public void deleteSessionPermanently(MessageReactionAddEvent event) {

        if (event.getTextChannel().getParent() != null) {
            Long channelId = event.getTextChannel().getParent().getIdLong();

            categoryService.findByCategoryId(channelId).ifPresent(category -> {
                Session session = sessionService.findByCategory(category);

                categoryService.delete(session.getCategory());

                if (session.getCharacters().size() > 0) {
                    characterService.delete(session.getCharacters());
                }

                sessionService.delete(session);

                event.getGuild().getRoleById(session.getRoleId()).delete().queue();
            });
        }
    }
}
