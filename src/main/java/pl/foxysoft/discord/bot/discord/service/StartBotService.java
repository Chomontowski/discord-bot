package pl.foxysoft.discord.bot.discord.service;


import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.JDA;
import org.springframework.stereotype.Component;
import pl.foxysoft.discord.bot.discord.listener.MainListener;
import pl.foxysoft.discord.bot.discord.listener.SessionDeletionReactionListener;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
@RequiredArgsConstructor
public class StartBotService {
    private final MainListener mainListener;
    private final SessionDeletionReactionListener sessionDeletionReactionListener;
    private final JDA jda;

    @PostConstruct
    public void startBot() {
        jda.addEventListener(mainListener);
        jda.addEventListener(sessionDeletionReactionListener);
    }

    private void shutdownBot() {
        jda.shutdown();
    }

    @PreDestroy
    public void onDestroy() {
        shutdownBot();
    }
}
