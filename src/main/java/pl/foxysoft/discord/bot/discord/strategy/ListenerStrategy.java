package pl.foxysoft.discord.bot.discord.strategy;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import pl.foxysoft.discord.bot.discord.listener.*;
import pl.foxysoft.discord.exception.CouldNotFindListenerException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ListenerStrategy {
    private final List<DiscordListener> listeners = new ArrayList<>();

    ListenerStrategy(RollListener rollListener,
                     SessionCreationListener sessionCreationListener,
                     SessionDeletionListener sessionDeletionListener,
                     TestListener testListener,
                     HelpListener helpListener,
                     ActivateSessionListener activateSessionListener,
                     DeactivateSessionListener deactivateSessionListener,
                     AddCharacterToSessionListener addCharacterToSessionListener) {
        listeners.add(rollListener);
        listeners.add(sessionCreationListener);
        listeners.add(sessionDeletionListener);
        listeners.add(testListener);
        listeners.add(helpListener);
        listeners.add(activateSessionListener);
        listeners.add(deactivateSessionListener);
        listeners.add(addCharacterToSessionListener);
    }

    public DiscordListener getProperListener(String message) throws CouldNotFindListenerException {
        String botCommand = message.split(" ")[0];
        log.debug("Bot command: " + botCommand);

        log.debug("Going to find listener class");
        return getListener(botCommand).orElseThrow(CouldNotFindListenerException::new);
    }

    @NotNull
    private Optional<DiscordListener> getListener(String botCommand) {
        return listeners.stream().filter(listener -> {
            String shortName = listener.getShortCommand();
            String longName = listener.getLongCommand();

            return botCommand.equals(shortName) || botCommand.equals(longName);
        }).findFirst();
    }
}
