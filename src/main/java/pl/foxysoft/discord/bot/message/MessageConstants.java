package pl.foxysoft.discord.bot.message;

public class MessageConstants {
    public static final String SESSION_CREATED_MESSAGE = "Good job! Your session is created! Be a good game master!";
    public static final String CHARACTER_CREATED = "Hey! %s just created character for you! You can edit it here: http://localhost:8080/character/%s";
    public static final String CAN_NOT_CREATE_SESSION = "I am sorry. I couldn't create your session. Please try again!";
    public static final String CAN_NOT_CREATE_CHARACTER = "I am sorry. I couldn't create your character. Please try again.";
    public static final String LISTENER_NOT_FOUND = "I don't recognize that command. Please try again";

    private MessageConstants() {
    }
}
