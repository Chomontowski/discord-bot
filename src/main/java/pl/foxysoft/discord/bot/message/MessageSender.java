package pl.foxysoft.discord.bot.message;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.springframework.stereotype.Component;
import pl.foxysoft.discord.service.ReactionMessageService;

@Component
@AllArgsConstructor
public class MessageSender {
    private static final String LINE_SEPARATOR = System.lineSeparator();
    private static final String SESSION_DELETION_INFO = "Are you sure you want to delete this session?";
    private static final String CHARACTER_DELETION_INFO = "Are you sure you want to delete that character?";

    private final JDA jda;
    private final ReactionMessageService reactionMessageService;


    public void sendChannelMessage(MessageReceivedEvent event, String text) {
        sendChannelMessage(event.getMessage().getChannel(), text);
    }

    public void sendChannelMessage(MessageChannel channel, String text) {
        channel.sendMessage(text).queue();
    }

    public void sendPrivateMessage(MessageReceivedEvent event, String text) {
        Member member = event.getMember();
        if (member != null) {
            this.sendPrivateMessage(member, text);
        }
    }

    public void sendPrivateMessage(Member member, String text) {
        sendPrivateMessage(member.getUser(), text);
    }

    public void sendPrivateMessage(Long discordId, String text) {
        User user = jda.getUserById(discordId);
        if ( user != null) {
            sendPrivateMessage(user, text);
        }
    }

    public void sendPrivateMessage(User user, String text) {
        user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessage(text).queue());
    }

    public void sendChannelMessageWithAuthorMention(MessageReceivedEvent event, String text) {
        sendChannelMessage(event, event.getAuthor().getAsMention() + LINE_SEPARATOR + text);
    }

    @SneakyThrows
    public void sendDeletionMessageWithReactions(MessageChannel channel, Long objectToDelete) {
        MessageAction channelAction;
        if(objectToDelete == null) {
            channelAction = channel.sendMessage(SESSION_DELETION_INFO);
        } else {
            channelAction = channel.sendMessage(CHARACTER_DELETION_INFO);
        }

        channelAction.queue(message -> {

            reactionMessageService.save(message.getIdLong(), objectToDelete);
            message.addReaction("\tU+1F1FE").queue();
            message.addReaction("\tU+1F1F3").queue();
        });
    }
}
