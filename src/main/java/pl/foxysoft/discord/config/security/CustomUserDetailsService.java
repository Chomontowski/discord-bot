package pl.foxysoft.discord.config.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.foxysoft.discord.config.security.oauth2.UserPrincipal;
import pl.foxysoft.discord.exception.ResourceNotFoundException;
import pl.foxysoft.discord.persistence.model.User;
import pl.foxysoft.discord.persistence.repository.UserRepository;

/**
 * Created by rajeevkumarsingh on 02/08/17.
 */

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() ->
                        new UsernameNotFoundException("User not found with email : " + email)
        );

        return UserPrincipal.create(user);
    }

    @Transactional
    public User loadUserById(Long id) {
        return userRepository.findByDiscordId(id).orElseThrow(
            () -> new ResourceNotFoundException("User", "id", id)
        );
    }
}
