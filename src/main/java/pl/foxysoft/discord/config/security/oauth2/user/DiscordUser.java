package pl.foxysoft.discord.config.security.oauth2.user;

import java.util.Map;

public class DiscordUser {
    protected Map<String, Object> attributes;


    public DiscordUser(Map<String, Object> attributes) {
        this.attributes = attributes;
    }


    public String getId() {
        return (String) attributes.get("id");
    }


    public String getName() {
        return (String) attributes.get("name");
    }


    public String getEmail() {
        return (String) attributes.get("email");
    }


//    public String getImageUrl() {
//        return (String) attributes.get("picture");
//    }
}
