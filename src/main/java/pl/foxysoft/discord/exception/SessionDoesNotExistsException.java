package pl.foxysoft.discord.exception;

public class SessionDoesNotExistsException extends Exception {
    public SessionDoesNotExistsException(String message) {
        super(message);
    }
}
