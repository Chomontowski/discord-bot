package pl.foxysoft.discord.exception;

public class SessionException extends Exception {
    public SessionException(String message) {
        super(message);
    }
}
