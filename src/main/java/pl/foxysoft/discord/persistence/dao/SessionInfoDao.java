package pl.foxysoft.discord.persistence.dao;

import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import pl.foxysoft.discord.persistence.model.SessionInfo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
@AllArgsConstructor
public class SessionInfoDao {

    private final JdbcTemplate jdbcTemplate;

    public List<SessionInfo> findByOwnerAndCharacterOwner(Long userId) {
        String query = "select `session`.id, `session`.active, `session`.name, `session`.owner, `session`.server, `session`.name, " +
                "COUNT(`character`.id) as 'total'," +
                "COUNT(`character`.accepted) as 'accepted'," +
                "SUM(`character`.done = 1) as 'done' " +
                "from `session` " +
                "join session_characters on session_characters.session_id = `session`.id " +
                "join `character` on `character`.id = session_characters.characters_id " +
                "where `character`.discord_id = ? or `session`.owner = ? " +
                "GROUP BY `session`.id";

        return jdbcTemplate.query(query, new Object[]{ userId, userId }, new SessionInfoRowMapper());
    }

    private static class SessionInfoRowMapper implements RowMapper<SessionInfo> {

        @Override
        public SessionInfo mapRow(ResultSet resultSet, int i) throws SQLException {
            SessionInfo sessionInfo = new SessionInfo();

            sessionInfo.setId(resultSet.getLong(1));
            sessionInfo.setActive(resultSet.getBoolean(2));
            sessionInfo.setSessionName(resultSet.getString(3));
            sessionInfo.setOwner(resultSet.getString(4));
            sessionInfo.setServer(resultSet.getString(5));
            sessionInfo.setName(resultSet.getString(6));
            sessionInfo.setTotalCharacters(resultSet.getInt(7));
            sessionInfo.setAcceptedCharacters(resultSet.getInt(8));
            sessionInfo.setCharactersToAccept(resultSet.getInt(9) - sessionInfo.getAcceptedCharacters());

            return sessionInfo;
        }
    }
}
