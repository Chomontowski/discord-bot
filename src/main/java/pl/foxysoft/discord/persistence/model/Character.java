package pl.foxysoft.discord.persistence.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "\"character\"")
public class Character {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long discordId;

    private Long serverId;

    private Boolean done;

    private Boolean accepted;

    @OneToOne
    private CharacterBaseInfo baseInfo;

    @OneToOne
    private CharacterAttributes attributes;

    @OneToMany
    private List<CharacterSkill> skills;

    @OneToMany
    private List<CharacterGadget> characterGadgets;

    @OneToMany
    private List<Trump> trumps;
}
