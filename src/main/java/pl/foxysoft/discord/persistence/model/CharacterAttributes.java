package pl.foxysoft.discord.persistence.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class CharacterAttributes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Short strength;
    private Short dexterity;
    private Short perspicacity;
    private Short calm;
    private Short charisma;
}
