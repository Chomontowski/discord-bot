package pl.foxysoft.discord.persistence.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class CharacterBaseInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String archetype;
    private String race;
    private String profession;
    private String nationality;

    @Column(length = 2000)
    private String history;
}
