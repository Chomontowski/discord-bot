package pl.foxysoft.discord.persistence.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class CharacterSkill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Skill skill;

    private Integer value;

    private String explanation;
}
