package pl.foxysoft.discord.persistence.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Session {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long owner;

    private Long server;

    private Boolean active;

    private String name;

    private Long roleId;

    @OneToOne(cascade = CascadeType.REMOVE)
    private Category category;

    @OneToMany
    private List<Character> characters;


}
