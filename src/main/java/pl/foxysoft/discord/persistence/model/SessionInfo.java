package pl.foxysoft.discord.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SessionInfo {

    private Long id;

    private String owner;
    private String server;
    private String name;
    private String serverName;
    private String sessionName;

    private Integer totalCharacters;
    private Integer acceptedCharacters;
    private Integer charactersToAccept;

    private Boolean active;
}
