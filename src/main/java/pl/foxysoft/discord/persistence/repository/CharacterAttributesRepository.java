package pl.foxysoft.discord.persistence.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import pl.foxysoft.discord.persistence.model.CharacterAttributes;

public interface CharacterAttributesRepository extends JpaRepository<CharacterAttributes, Long> {

}
