package pl.foxysoft.discord.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.foxysoft.discord.persistence.model.CharacterBaseInfo;

public interface CharacterBaseInfoRepository extends JpaRepository<CharacterBaseInfo, Long> {
}
