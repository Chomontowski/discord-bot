package pl.foxysoft.discord.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.foxysoft.discord.persistence.model.CharacterSkill;

public interface CharacterSkillRepository extends JpaRepository<CharacterSkill, Long> {
}
