package pl.foxysoft.discord.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.foxysoft.discord.persistence.model.ReactionMessage;

public interface ReactionMessageRepository extends JpaRepository<ReactionMessage, Long> {
}
