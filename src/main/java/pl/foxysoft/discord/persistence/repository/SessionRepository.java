package pl.foxysoft.discord.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.foxysoft.discord.persistence.model.Category;
import pl.foxysoft.discord.persistence.model.Character;
import pl.foxysoft.discord.persistence.model.Session;

import java.util.List;
import java.util.Optional;

public interface SessionRepository extends JpaRepository<Session, Long> {
    Optional<Session> findByOwnerAndServer(Long owner, Long server);

    Optional<Session> findByCharactersIn(List<Character> character);

    Optional<Session> findByServerAndName(Long server, String name);

    Optional<Session> findByCategory(Category category);
}
