package pl.foxysoft.discord.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.foxysoft.discord.persistence.model.Skill;

public interface SkillRepository extends JpaRepository<Skill, Long> {
}
