package pl.foxysoft.discord.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.foxysoft.discord.persistence.model.Trump;

public interface TrumpRepository extends JpaRepository<Trump, Long> {
}
