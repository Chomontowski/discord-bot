package pl.foxysoft.discord.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.foxysoft.discord.persistence.model.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

    Boolean existsByEmail(String email);

    Optional<User> findByDiscordId(Long id);
}
