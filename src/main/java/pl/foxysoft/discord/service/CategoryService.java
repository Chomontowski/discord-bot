package pl.foxysoft.discord.service;

import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildChannel;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.requests.restaction.ChannelAction;
import org.springframework.stereotype.Service;
import pl.foxysoft.discord.persistence.model.Category;
import pl.foxysoft.discord.persistence.repository.CategoryRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CategoryService {

    private static final List<Permission> roleAllowedPermissions = List.of(
            Permission.VIEW_CHANNEL,
            Permission.MESSAGE_WRITE,
            Permission.MANAGE_CHANNEL,
            Permission.MANAGE_PERMISSIONS,
            Permission.MANAGE_WEBHOOKS,
            Permission.MESSAGE_MENTION_EVERYONE,
            Permission.MESSAGE_TTS,
            Permission.MESSAGE_MANAGE,
            Permission.VOICE_SPEAK,
            Permission.VOICE_CONNECT,
            Permission.VOICE_MUTE_OTHERS,
            Permission.VOICE_DEAF_OTHERS,
            Permission.VOICE_MOVE_OTHERS,
            Permission.VOICE_USE_VAD

    );

    private static final List<Permission> roleDeniedPermissions = List.of(
            Permission.MESSAGE_EMBED_LINKS,
            Permission.MESSAGE_ATTACH_FILES,
            Permission.MESSAGE_HISTORY,
            Permission.MESSAGE_EXT_EMOJI,
            Permission.MESSAGE_ADD_REACTION,
            Permission.VOICE_STREAM,
            Permission.VOICE_SPEAK
    );

    private static final List<Permission> everyoneDeniedPermissions = List.of(
            Permission.CREATE_INSTANT_INVITE,
            Permission.MANAGE_CHANNEL,
            Permission.MANAGE_PERMISSIONS,
            Permission.MANAGE_WEBHOOKS,
            Permission.MESSAGE_WRITE,
            Permission.MESSAGE_TTS,
            Permission.MESSAGE_MANAGE,
            Permission.MESSAGE_EMBED_LINKS,
            Permission.MESSAGE_ATTACH_FILES,
            Permission.MESSAGE_MENTION_EVERYONE,
            Permission.MESSAGE_EXT_EMOJI,
            Permission.VOICE_SPEAK,
            Permission.VOICE_MUTE_OTHERS,
            Permission.VOICE_DEAF_OTHERS,
            Permission.VOICE_MOVE_OTHERS,
            Permission.VOICE_USE_VAD,
            Permission.VOICE_STREAM
            );

    private final CategoryRepository categoryRepository;
    private final JDA jda;


    public Category createCategory(Guild guild, Role role, String sessionName) {
        Category category = new Category();

        ChannelAction<net.dv8tion.jda.api.entities.Category> action = guild.createCategory(sessionName);

        action = createPermissionsForSessionRole(action, role);
        action = createPermissionsForEveryone(guild, action);

        action.queue(c -> {
            category.setCategoryId(c.getIdLong());
            c.createTextChannel("rzuty").queue();
            c.createTextChannel("notatki").queue();
            c.createTextChannel("off-top").queue();
            c.createVoiceChannel("Sesja").queue();

            categoryRepository.save(category);
        });

        return categoryRepository.save(category);
    }

    private ChannelAction<net.dv8tion.jda.api.entities.Category> createPermissionsForSessionRole(ChannelAction<net.dv8tion.jda.api.entities.Category> category, Role sessionRole) {
        return category.addPermissionOverride(
                sessionRole,
                roleAllowedPermissions,
                roleDeniedPermissions
        );
    }

    private ChannelAction<net.dv8tion.jda.api.entities.Category> createPermissionsForEveryone(Guild guild, ChannelAction<net.dv8tion.jda.api.entities.Category> category) {
        return category.addPermissionOverride(
                guild.getPublicRole(),
                new ArrayList<>(),
                everyoneDeniedPermissions
        );
    }

    public void delete(Category category) {
        List<GuildChannel> channels = jda.getCategoryById(category.getCategoryId()).getChannels();
        channels.forEach(channel -> channel.delete().queue());
        jda.getCategoryById(category.getCategoryId()).delete().queue();
    }

    public Optional<Category> findByCategoryId(Long categoryId) {
        return categoryRepository.findByCategoryId(categoryId);
    }

}
