package pl.foxysoft.discord.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.foxysoft.discord.persistence.model.CharacterAttributes;
import pl.foxysoft.discord.persistence.repository.CharacterAttributesRepository;

@Service
@AllArgsConstructor
public class CharacterAttributesService {
    private final CharacterAttributesRepository characterAttributesRepository;

    public CharacterAttributes save(CharacterAttributes attributes) {
        return characterAttributesRepository.save(attributes);
    }

    public void delete(CharacterAttributes attributes) {
        characterAttributesRepository.delete(attributes);
    }
}
