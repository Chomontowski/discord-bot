package pl.foxysoft.discord.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.foxysoft.discord.persistence.model.CharacterBaseInfo;
import pl.foxysoft.discord.persistence.repository.CharacterBaseInfoRepository;

@Service
@AllArgsConstructor
public class CharacterBaseInfoService {

    private final CharacterBaseInfoRepository characterBaseInfoRepository;

    public CharacterBaseInfo save(CharacterBaseInfo characterBaseInfo) {
        return characterBaseInfoRepository.save(characterBaseInfo);
    }

    public void delete(CharacterBaseInfo baseInfo) {
        characterBaseInfoRepository.delete(baseInfo);
    }
}
