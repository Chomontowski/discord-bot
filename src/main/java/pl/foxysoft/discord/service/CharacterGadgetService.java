package pl.foxysoft.discord.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.foxysoft.discord.persistence.model.CharacterGadget;
import pl.foxysoft.discord.persistence.repository.CharacterGadgetRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class CharacterGadgetService {
    private final CharacterGadgetRepository characterGadgetRepository;


    public List<CharacterGadget> saveAll(List<CharacterGadget> characterGadgets) {
        return characterGadgetRepository.saveAll(characterGadgets);
    }

    public CharacterGadget save(CharacterGadget characterGadget) {
        return characterGadgetRepository.save(characterGadget);
    }

    public void delete(List<CharacterGadget> characterGadgets) {
        characterGadgetRepository.deleteAll(characterGadgets);
    }
}
