package pl.foxysoft.discord.service;

import net.dv8tion.jda.api.entities.Member;
import pl.foxysoft.discord.persistence.model.Character;
import pl.foxysoft.discord.web.dto.CharacterDTO;

import java.util.List;

public interface CharacterService extends UserSecurityService {

    List<CharacterDTO> getCharacters();

    CharacterDTO getCharacterById(Long id);

    CharacterDTO saveCharacter(Character character);

    void delete(List<Character> characters);

    List<Character> createCharacters(List<Member> members, String serverId);
}
