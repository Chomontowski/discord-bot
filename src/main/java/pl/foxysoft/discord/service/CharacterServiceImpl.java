package pl.foxysoft.discord.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Member;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import pl.foxysoft.discord.persistence.model.Character;
import pl.foxysoft.discord.persistence.model.CharacterSkill;
import pl.foxysoft.discord.persistence.model.Skill;
import pl.foxysoft.discord.persistence.repository.CharacterRepository;
import pl.foxysoft.discord.web.dto.CharacterDTO;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CharacterServiceImpl implements CharacterService {

    private final CharacterRepository characterRepository;
    private final CharacterBaseInfoService characterBaseInfoService;
    private final CharacterAttributesService characterAttributesService;
    private final CharacterSkillService characterSkillService;

    private final CharacterGadgetService characterGadgetService;
    private final ModelMapper modelMapper;
    private final SkillService skillService;
    private final JDA jda;

    @Override
    @Transactional
    public List<CharacterDTO> getCharacters() {
        List<CharacterDTO> characterDTOS = characterRepository.findByDiscordId(getUserId())
                .stream()
                .map(character -> modelMapper.map(character, CharacterDTO.class))
                .collect(Collectors.toList());

        characterDTOS.forEach(this::addServerAndSessionName);

        return characterDTOS;
    }

    @Override
    @Transactional
    public CharacterDTO getCharacterById(Long id) {
        Optional<Character> maybeCharacter = characterRepository.findById(id);
        CharacterDTO characterDTO = null;
        if (maybeCharacter.isPresent()) {
            characterDTO = modelMapper.map(maybeCharacter.get(), CharacterDTO.class);
            addServerAndSessionName(characterDTO);
        }

        return characterDTO;
    }

    @Override
    @Transactional
    public CharacterDTO saveCharacter(Character character) {
        character.setBaseInfo(characterBaseInfoService.save(character.getBaseInfo()));
        character.setAttributes(characterAttributesService.save(character.getAttributes()));
        character.setSkills(characterSkillService.saveAll(character.getSkills()));
        character.setCharacterGadgets(characterGadgetService.saveAll(character.getCharacterGadgets()));

        character.setDone(true);
        character = characterRepository.save(character);
        CharacterDTO characterDTO = modelMapper.map(character, CharacterDTO.class);
        addServerAndSessionName(characterDTO);

        return characterDTO;
    }

    @Override
    public void delete(List<Character> characters) {
        characters.forEach(this::deleteDependencies);

        characterRepository.deleteAll(characters);
    }

    private void deleteDependencies(Character character) {
        if(character.getBaseInfo() != null) {
            characterBaseInfoService.delete(character.getBaseInfo());
        }

        if(character.getSkills().size() > 0) {
            characterSkillService.delete(character.getSkills());
        }

        if(character.getAttributes() != null) {
            characterAttributesService.delete(character.getAttributes());
        }

        if(character.getCharacterGadgets().size() > 0) {
            characterGadgetService.delete(character.getCharacterGadgets());
        }
    }


    private void addServerAndSessionName(CharacterDTO characterDTO) {
        characterDTO.setServerName(jda.getGuildById(characterDTO.getServerId()).getName());
//        characterDTO.setChannelName(jda.getTextChannelById(characterDTO.getChannelId()).getName());
    }

    @Override
    @Transactional
    public List<Character> createCharacters(List<Member> members, String serverId) {
        return members.stream().map(member -> initializeCharacter(member, serverId)).collect(Collectors.toList());
    }

    private Character initializeCharacter(Member member, String serverId) {
        Character character = new Character();
        character.setDiscordId(member.getIdLong());
        character.setServerId(Long.valueOf(serverId));
        character.setSkills(initializeSkills(skillService.getAll()));
        character.setDone(false);
        return characterRepository.save(character);
    }

    private List<CharacterSkill> initializeSkills(List<Skill> skills) {
        List<CharacterSkill> characterSkills = skills.stream().map(skill -> {
            CharacterSkill characterSkill = new CharacterSkill();
            characterSkill.setSkill(skill);
            characterSkill.setValue(3);

            return characterSkill;
        }).collect(Collectors.toList());

        return characterSkillService.saveAll(characterSkills);
    }
}
