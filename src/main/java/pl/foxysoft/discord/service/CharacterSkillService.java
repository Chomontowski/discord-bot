package pl.foxysoft.discord.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.foxysoft.discord.persistence.model.CharacterSkill;
import pl.foxysoft.discord.persistence.repository.CharacterSkillRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class CharacterSkillService {

    private final CharacterSkillRepository characterSkillRepository;

    public List<CharacterSkill> saveAll(List<CharacterSkill> skills) {
        return characterSkillRepository.saveAll(skills);
    }

    public void delete(List<CharacterSkill> skills) {
        characterSkillRepository.deleteAll(skills);
    }
}
