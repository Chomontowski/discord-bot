package pl.foxysoft.discord.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.foxysoft.discord.persistence.model.ReactionMessage;
import pl.foxysoft.discord.persistence.repository.ReactionMessageRepository;

import java.util.Optional;

@Service
@AllArgsConstructor
public class ReactionMessageService {

    private final ReactionMessageRepository reactionMessageRepository;

    public Optional<ReactionMessage> save(Long messageId, Long objectToDelete) {
        ReactionMessage reactionMessage = new ReactionMessage();

        reactionMessage.setMessageId(messageId);
        reactionMessage.setObjectToDeleteId(objectToDelete);

        return Optional.of(reactionMessageRepository.save(reactionMessage));
    }

    public void delete(ReactionMessage reactionMessage) {
        reactionMessageRepository.delete(reactionMessage);
    }
}
