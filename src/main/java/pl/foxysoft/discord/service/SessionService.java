package pl.foxysoft.discord.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import pl.foxysoft.discord.exception.SessionDoesNotExistsException;
import pl.foxysoft.discord.exception.SessionException;
import pl.foxysoft.discord.persistence.dao.SessionInfoDao;
import pl.foxysoft.discord.persistence.model.Category;
import pl.foxysoft.discord.persistence.model.Character;
import pl.foxysoft.discord.persistence.model.Session;
import pl.foxysoft.discord.persistence.repository.SessionRepository;
import pl.foxysoft.discord.web.dto.CharacterDTO;
import pl.foxysoft.discord.web.dto.SessionDTO;
import pl.foxysoft.discord.web.dto.SessionInfoDTO;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class SessionService implements UserSecurityService {

    private final SessionRepository sessionRepository;
    private final SessionInfoDao sessionInfoDao;
    private final ModelMapper modelMapper;
    private final CategoryService categoryService;
    private final CharacterService characterService;
    private final JDA jda;

    public Session saveSession(Session session) {
        return sessionRepository.save(session);
    }

    public Session createSession(Member owner, String serverId, Category category, String sessionName, Long roleId) {

        Long ownerId = Long.valueOf(owner.getId());
        Session session = new Session();
        session.setOwner(ownerId);
        session.setServer(Long.valueOf(serverId));
        session.setName(sessionName);
        session.setActive(true);
        session.setCategory(category);
        session.setRoleId(roleId);
        return sessionRepository.save(session);
    }

    private Optional<Session> sessionCheck(Member owner) {
        return sessionRepository
                .findByOwnerAndServer(owner.getIdLong(), owner.getGuild().getIdLong());
    }

    public Optional<Session> sessionCheck(Guild guild, String sessionName) {
        return sessionRepository
                .findByServerAndName(guild.getIdLong(), sessionName);
    }

    public SessionDTO getSessionDetails(Long sessionId) {
        Session session = sessionRepository.findById(sessionId).orElseThrow();

        SessionDTO sessionDTO = modelMapper.map(session, SessionDTO.class);

        addServerAndChannelName(sessionDTO);

        return sessionDTO;
    }

    public void openSession(Member owner) throws SessionDoesNotExistsException {
        Optional<Session> maybeSession = sessionCheck(owner);

        if(maybeSession.isPresent()) {
            Session session = maybeSession.get();
            session.setActive(true);
            sessionRepository.save(session);
        } else {
            throw new SessionDoesNotExistsException("Session for this server and channel does not exists! Try on another channel!");
        }
    }

    public List<SessionInfoDTO> getAllSessionInfos() {
        Long userId = getUserId();

        List<SessionInfoDTO> sessions = sessionInfoDao.findByOwnerAndCharacterOwner(userId)
                .stream()
                .map(sessionInfo -> modelMapper.map(sessionInfo, SessionInfoDTO.class))
                .collect(Collectors.toList());

        sessions.forEach(this::addServerAndChannelName);

        return sessions;
    }

    public void deactivateSession(Long categoryId, User author) throws SessionException {
        Session session = findSessionByDiscordCategoryId(categoryId);
        checkSessionOwner(author, session.getOwner());

        if(!session.getActive()) {
            throw new SessionException(author.getAsMention() + " Hey! Session is not active already!");
        }

        session.setActive(false);

        sessionRepository.save(session);
    }

    public void activateSession(Long categoryId, User author) throws SessionException {
        Session session = findSessionByDiscordCategoryId(categoryId);
        checkSessionOwner(author, session.getOwner());

        if(session.getActive()) {
            throw new SessionException(author.getAsMention() + " Hey! Session is already active!");
        }

        session.setActive(true);

        sessionRepository.save(session);
    }

    public void checkSessionOwnership(User author, Long discordCategoryId) throws SessionException {
        Session session = findSessionByDiscordCategoryId(discordCategoryId);

        checkSessionOwner(author, session.getOwner());
    }

    @Transactional
    public void addCharactersToSession(List<Member> members, Long discordCategoryId, String serverId) throws SessionException {
        Session session = findSessionByDiscordCategoryId(discordCategoryId);

        List<Character> characters = characterService.createCharacters(members, serverId);

        session.getCharacters().addAll(characters);

        sessionRepository.save(session);
    }

    private void checkSessionOwner(User author, Long sessionOwnerId) throws SessionException {
        if(!(Objects.equals(author.getIdLong(), sessionOwnerId))) {
            throw new SessionException(author.getAsMention() + " You are not allowed to do this!");
        }
    }

    public Session findSessionByDiscordCategoryId(Long categoryId) throws SessionException {
        Supplier<SessionException> sessionDoesNotExists = () -> new SessionException("You don't have any session under that category!");
        Category category = categoryService.findByCategoryId(categoryId).orElseThrow(sessionDoesNotExists);

        return sessionRepository.findByCategory(category).orElseThrow(sessionDoesNotExists);
    }

    private void addServerAndChannelName(SessionInfoDTO sessionInfoDTO) {
        sessionInfoDTO.setServerName(jda.getGuildById(sessionInfoDTO.getServer()).getName());
    }

    private void addServerAndChannelName(SessionDTO sessionDTO) {
        sessionDTO.setServerName(jda.getGuildById(sessionDTO.getServer()).getName());

        sessionDTO.getCharacters().forEach(character -> addServerAndChannelName(character, sessionDTO.getName()));
    }

    private void addServerAndChannelName(CharacterDTO characterDTO, String sessionName) {
        characterDTO.setServerName(jda.getGuildById(characterDTO.getServerId()).getName());
        characterDTO.setSessionName(sessionName);
    }

    public Session findByCategory(Category category) {
        return sessionRepository.findByCategory(category).orElseThrow();
    }

    public void delete(Session session) {
        sessionRepository.delete(session);
    }
}
