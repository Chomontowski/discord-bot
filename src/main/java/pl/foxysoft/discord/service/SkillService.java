package pl.foxysoft.discord.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.foxysoft.discord.persistence.model.Skill;
import pl.foxysoft.discord.persistence.repository.SkillRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class SkillService {

    private final SkillRepository skillRepository;

    public List<Skill> getAll() {
        return skillRepository.findAll();
    }

    public List<Skill> getByIds(List<Long> ids) {
        return skillRepository.findAllById(ids);
    }

}
