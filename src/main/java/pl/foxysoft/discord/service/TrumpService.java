package pl.foxysoft.discord.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.foxysoft.discord.persistence.model.Trump;
import pl.foxysoft.discord.persistence.repository.TrumpRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class TrumpService {

    private final TrumpRepository trumpRepository;

    public List<Trump> findAll() {
        return trumpRepository.findAll();
    }

    public List<Trump> findAllByIds(List<Long> trumpsIds) {
        return trumpRepository.findAllById(trumpsIds);
    }
}
