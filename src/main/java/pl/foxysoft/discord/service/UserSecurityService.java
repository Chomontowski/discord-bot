package pl.foxysoft.discord.service;

import org.springframework.security.core.context.SecurityContextHolder;
import pl.foxysoft.discord.config.security.oauth2.UserPrincipal;

public interface UserSecurityService {

    default Long getUserId() {
        var user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ((UserPrincipal) user).getId();
    }
}
