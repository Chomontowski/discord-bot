package pl.foxysoft.discord.web;

import lombok.AllArgsConstructor;
import lombok.val;
import net.dv8tion.jda.api.JDA;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import pl.foxysoft.discord.persistence.model.*;
import pl.foxysoft.discord.persistence.model.Character;
import pl.foxysoft.discord.service.SkillService;
import pl.foxysoft.discord.web.dto.*;
import pl.foxysoft.discord.web.dto.character.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
@AllArgsConstructor
public class DtoMapper {

    private final JDA jda;
    private final SkillService skillService;

    public List<TrumpDTO> mapToTrumpsDto(List<Trump> trumps) {
        return trumps.stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public TrumpDTO mapToDto(Trump trump) {
        return new TrumpDTO(trump.getId(), trump.getName(), trump.getDescription(), trump.getPrice());
    }

    public List<CharacterGadgetDTO> mapToGadgetsDto(List<CharacterGadget> gadgets) {
        return gadgets.stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public CharacterGadgetDTO mapToDto(CharacterGadget gadget) {
        return new CharacterGadgetDTO(gadget.getId(), gadget.getName(), mapToSkillsDto(gadget.getSkills()));
    }

    public List<SkillDTO> mapToSkillsDto(List<Skill> skills) {
        return skills.stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public SkillDTO mapToDto(Skill skill) {
        return new SkillDTO(skill.getId(), skill.getName());
    }

//    public List<CharacterSpecializedSkillDTO> mapToSpecializedSkillsDto(List<CharacterSpecializedSkill> skills) {
//        return skills.stream().map(this::mapToDto).collect(Collectors.toList());
//    }

//    public CharacterSpecializedSkillDTO mapToDto(CharacterSpecializedSkill skill) {
//        return new CharacterSpecializedSkillDTO(skill.getId(), skill.getName(), skill.getInformation());
//    }

//    public List<CharacterSkillDTO> mapToDto(List<CharacterSkill> characterSkills) {
//        return characterSkills.stream().map(this::mapToDto).collect(Collectors.toList());
//    }

//    public CharacterSkillDTO mapToDto(CharacterSkill characterSkill) {
//        return new CharacterSkillDTO(characterSkill.getId(), characterSkill.getName());
//    }

    public CharacterAttributesDTO mapToDto(CharacterAttributes attributes) {
        return new CharacterAttributesDTO(
                attributes.getStrength(),
                attributes.getDexterity(),
                attributes.getPerspicacity(),
                attributes.getCalm(),
                attributes.getCharisma()
        );
    }

    public CharacterBaseInfoDTO mapToDto(CharacterBaseInfo characterBaseInfo) {
        return new CharacterBaseInfoDTO(
                characterBaseInfo.getName() != null ? characterBaseInfo.getName() : "",
                characterBaseInfo.getArchetype() != null ? characterBaseInfo.getArchetype() : "",
                characterBaseInfo.getRace() != null ? characterBaseInfo.getRace() : "",
                characterBaseInfo.getProfession() != null ? characterBaseInfo.getProfession() : "",
                characterBaseInfo.getNationality() != null ? characterBaseInfo.getNationality() : "",
                characterBaseInfo.getHistory() != null ? characterBaseInfo.getHistory() : ""
        );
    }


}
