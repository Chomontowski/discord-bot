package pl.foxysoft.discord.web.controller;

import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.foxysoft.discord.persistence.model.Character;
import pl.foxysoft.discord.service.CharacterService;
import pl.foxysoft.discord.web.dto.CharacterDTO;

import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/characters")
public class CharacterController {

    private final CharacterService characterService;
    private final ModelMapper modelMapper = new ModelMapper();

    public CharacterController(CharacterService characterService) {
        this.characterService = characterService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CharacterDTO>> getAllCharacters() {
        return ResponseEntity.ok(characterService.getCharacters());
    }

    @GetMapping(value = "/{id}/character", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CharacterDTO> getCharacterById(@PathVariable Long id) {
        return ResponseEntity.ok(characterService.getCharacterById(id));
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CharacterDTO> saveCharacter(@RequestBody CharacterDTO characterDTO) {
        Character character = modelMapper.map(characterDTO, Character.class);

        return ResponseEntity.ok(characterService.saveCharacter(character));
    }
}
