package pl.foxysoft.discord.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

//    OAuth2AuthenticationToken token, Principal principal, Authentication authentication

    @GetMapping("/")
    public String hello() {
        return "home";
    }
}
