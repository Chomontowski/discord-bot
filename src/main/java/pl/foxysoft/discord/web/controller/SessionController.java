package pl.foxysoft.discord.web.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.foxysoft.discord.service.SessionService;
import pl.foxysoft.discord.web.dto.SessionDTO;
import pl.foxysoft.discord.web.dto.SessionInfoDTO;

import java.util.List;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/sessions")
public class SessionController {

    private final SessionService sessionService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SessionInfoDTO>> getAllSessionInfos() {
        return ResponseEntity.ok(sessionService.getAllSessionInfos());
    }

    @GetMapping(value = "/{sessionId}/details")
    public ResponseEntity<SessionDTO> getSessionDetails(@PathVariable Long sessionId) {
        return ResponseEntity.ok(sessionService.getSessionDetails(sessionId));
    }
}
