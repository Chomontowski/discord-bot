package pl.foxysoft.discord.web.controller;

import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.foxysoft.discord.service.TrumpService;
import pl.foxysoft.discord.web.dto.character.TrumpDTO;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/trumps")
public class TrumpController {

    private final TrumpService trumpService;
    private final ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<List<TrumpDTO>> getAllTrumps() {
        return ResponseEntity.ok(trumpService.findAll()
                .stream()
                .map(trump -> modelMapper.map(trump, TrumpDTO.class))
                .collect(Collectors.toList())
        );
    }
}
