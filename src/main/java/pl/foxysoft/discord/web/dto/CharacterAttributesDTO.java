package pl.foxysoft.discord.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CharacterAttributesDTO {
    private Short strength;
    private Short dexterity;
    private Short perspicacity;
    private Short calm;
    private Short charisma;
}
