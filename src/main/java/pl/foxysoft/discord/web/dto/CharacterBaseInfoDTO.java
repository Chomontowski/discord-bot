package pl.foxysoft.discord.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CharacterBaseInfoDTO {
    private String name;
    private String archetype;
    private String race;
    private String profession;
    private String nationality;
    private String history;
}
