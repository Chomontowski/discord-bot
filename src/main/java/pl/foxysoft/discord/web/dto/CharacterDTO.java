package pl.foxysoft.discord.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.foxysoft.discord.web.dto.character.CharacterGadgetDTO;
import pl.foxysoft.discord.web.dto.character.CharacterSkillDTO;
import pl.foxysoft.discord.web.dto.character.TrumpDTO;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CharacterDTO {

    private Long id;
    private String discordId;
    private String serverId;
    private Boolean done;
    private Boolean accepted;
    private CharacterBaseInfoDTO baseInfo;
    private CharacterAttributesDTO attributes;

    private String serverName;
    private String sessionName;

    private List<CharacterSkillDTO> skills;
    private List<CharacterGadgetDTO> characterGadgets;
    private List<TrumpDTO> trumps;


}
