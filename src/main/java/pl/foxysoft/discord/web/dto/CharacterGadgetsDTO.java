package pl.foxysoft.discord.web.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CharacterGadgetsDTO {

    private List<String> gadget;
    private List<Long> attribute1;
    private List<Long> attribute2;
    private List<Long> attribute3;
    private List<Long> attribute4;

}
