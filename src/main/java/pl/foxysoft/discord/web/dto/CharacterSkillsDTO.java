package pl.foxysoft.discord.web.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CharacterSkillsDTO {
    List<String> skill;
    List<String> specializedSkill;
    List<String> specializedSkillInfo;
}
