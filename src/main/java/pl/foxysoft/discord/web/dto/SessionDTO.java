package pl.foxysoft.discord.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SessionDTO {

    private Long id;

    private String owner;

    private String server;

    private Boolean active;
    private String name;
    private List<CharacterDTO> characters;

    private String serverName;
    private String channelName;
}
