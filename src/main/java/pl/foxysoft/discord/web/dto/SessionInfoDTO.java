package pl.foxysoft.discord.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SessionInfoDTO {
    private Long id;

    private String owner;
    private String server;
    private String sessionName;

    private String serverName;
    private String name;
    private Integer totalCharacters;
    private Integer acceptedCharacters;
    private Integer charactersToAccept;

    private Boolean active;
}
