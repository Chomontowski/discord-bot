package pl.foxysoft.discord.web.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class TrumpsDTO {

    private List<Long> trump;
}
