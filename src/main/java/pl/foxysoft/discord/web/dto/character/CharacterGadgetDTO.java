package pl.foxysoft.discord.web.dto.character;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CharacterGadgetDTO {
    private Long id;
    private String name;
    private List<SkillDTO> skills;
}
