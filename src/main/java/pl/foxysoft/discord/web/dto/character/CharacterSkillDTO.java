package pl.foxysoft.discord.web.dto.character;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CharacterSkillDTO {
    private Long id;
    private SkillDTO skill;
    private Integer value;
    private String explanation;
}
