INSERT INTO skill(name) VALUES('ANALIZA (Prz)');
INSERT INTO skill(name) VALUES('BLEF (Cha)');
INSERT INTO skill(name) VALUES('EKSPRESJA (Cha)');
INSERT INTO skill(name) VALUES('EMPATIA (Prz)');
INSERT INTO skill(name) VALUES('ODWAGA (Op)');
INSERT INTO skill(name) VALUES('OKULTYZM (Prz)');
INSERT INTO skill(name) VALUES('PERSWAZJA (Cha)');
INSERT INTO skill(name) VALUES('POJAZDY (Zr)');
INSERT INTO skill(name) VALUES('PÓŁŚWIATEK (Prz)');
INSERT INTO skill(name) VALUES('PRZYRODA (Prz)');
INSERT INTO skill(name) VALUES('SPOSTRZEGAWCZOŚĆ (Prz)');
INSERT INTO skill(name) VALUES('STRZELANIE (Zr)');
INSERT INTO skill(name) VALUES('TECHNIKA (Prz)');
INSERT INTO skill(name) VALUES('UKRYWANIE SIĘ (Zr)');
INSERT INTO skill(name) VALUES('WALKA (Krz)');
INSERT INTO skill(name) VALUES('WIEDZA AKADEMICKA (Prz)');
INSERT INTO skill(name) VALUES('WYSPORTOWANIE (Krz)');
INSERT INTO skill(name) VALUES('ZASTRASZENIE (Cha)');
INSERT INTO skill(name) VALUES('ZŁODZIEJSTWO (Zr)');

INSERT INTO trump(name,description, price) VALUES ('Pewne oko, pewna ręka', '', 5);
INSERT INTO trump(name,description, price) VALUES ('Szybkie dobycie', '', 5);
INSERT INTO trump(name,description, price) VALUES ('Cień', '', 5);
INSERT INTO trump(name,description, price) VALUES ('Naganiacz', '', 5);
INSERT INTO trump(name,description, price) VALUES ('Ogar', '', 5);
INSERT INTO trump(name,description, price) VALUES ('Szybkie reakcje', '', 5);
INSERT INTO trump(name,description, price) VALUES ('Język obcy (martwy, kolonialny lub egzotyczny)', '', 5);
INSERT INTO trump(name,description, price) VALUES ('Nadzwyczajna kompetencja', '', 5);
INSERT INTO trump(name,description, price) VALUES ('Popisowy numer', '', 5);

INSERT INTO trump(name, description, price) VALUES('Celny cios', '', 10);
INSERT INTO trump(name, description, price) VALUES('Nieustępliwe natarcie', '', 10);
INSERT INTO trump(name, description, price) VALUES('Oburęczność', '', 10);
INSERT INTO trump(name, description, price) VALUES('Parada', '', 10);
INSERT INTO trump(name, description, price) VALUES('Rozbrojenie', '', 10);
INSERT INTO trump(name, description, price) VALUES('Unik', '', 10);
INSERT INTO trump(name, description, price) VALUES('Zapaśnik', '', 10);
INSERT INTO trump(name, description, price) VALUES('Akrobata', '', 10);
INSERT INTO trump(name, description, price) VALUES('Bez sentymentów', '', 10);
INSERT INTO trump(name, description, price) VALUES('Mistrz kierownicy', '', 10);
INSERT INTO trump(name, description, price) VALUES('Niesamowity refleks', '', 10);
INSERT INTO trump(name, description, price) VALUES('Niespodziewany manewr', '', 10);
INSERT INTO trump(name, description, price) VALUES('Nieprzewidywalny', '', 10);
INSERT INTO trump(name, description, price) VALUES('Ryzykowny manewr', '', 10);
INSERT INTO trump(name, description, price) VALUES('Żywe srebro', '', 10);
INSERT INTO trump(name, description, price) VALUES('Agresywna dyskusja', '', 10);
INSERT INTO trump(name, description, price) VALUES('Bezczelny kłamca', '', 10);
INSERT INTO trump(name, description, price) VALUES('Celna riposta', '', 10);
INSERT INTO trump(name, description, price) VALUES('Gra na emocjach', '', 10);
INSERT INTO trump(name, description, price) VALUES('Kontrargument', '', 10);
INSERT INTO trump(name, description, price) VALUES('Krasomówca', '', 10);
INSERT INTO trump(name, description, price) VALUES('Niepodważalny argument', '', 10);
INSERT INTO trump(name, description, price) VALUES('Słaby punkt', '', 10);
INSERT INTO trump(name, description, price) VALUES('Zawoalowana groźba', '', 10);
INSERT INTO trump(name, description, price) VALUES('Rozpoznanie sytuacji', '', 10);
INSERT INTO trump(name, description, price) VALUES('Sojusznik', '', 10);
INSERT INTO trump(name, description, price) VALUES('Wyzwanie', '', 10);

INSERT INTO trump(name, description, price) VALUES('Niepokorny', '', 15);
INSERT INTO trump(name, description, price) VALUES('Regeneracja', '', 15);
INSERT INTO trump(name, description, price) VALUES('Silny cios', '', 15);
INSERT INTO trump(name, description, price) VALUES('Zamaszysty cios', '', 15);
INSERT INTO trump(name, description, price) VALUES('Chwila oddechu', '', 15);
INSERT INTO trump(name, description, price) VALUES('Nagłe przyspieszenie', '', 15);
INSERT INTO trump(name, description, price) VALUES('Szósty zmysł', '', 15);
INSERT INTO trump(name, description, price) VALUES('Zastrzyk adrenaliny', '', 15);
INSERT INTO trump(name, description, price) VALUES('Miażdżąca krytyka', '', 15);
INSERT INTO trump(name, description, price) VALUES('Uroczy', '', 15);
INSERT INTO trump(name, description, price) VALUES('Wewnętrzne przekonanie', '', 15);
INSERT INTO trump(name, description, price) VALUES('Moc magiczna (rzuć w Tabeli nr 9 na Moc)', '', 15);
INSERT INTO trump(name, description, price) VALUES('Tworzenie magicznego przedmiotu', '', 15);
INSERT INTO trump(name, description, price) VALUES('Wzrost Kondycji', '', 15);
INSERT INTO trump(name, description, price) VALUES('Wzrost Reputacji', '', 15);

INSERT INTO trump(name, description, price) VALUES('Finta', '', 20);
INSERT INTO trump(name, description, price) VALUES('Nie do zabicia', '', 20);
INSERT INTO trump(name, description, price) VALUES('Sekretny cios', '', 20);
INSERT INTO trump(name, description, price) VALUES('Kaskader', '', 20);
INSERT INTO trump(name, description, price) VALUES('Szaleńczy manewr', '', 20);
INSERT INTO trump(name, description, price) VALUES('Mistrz retoryki', '', 20);
INSERT INTO trump(name, description, price) VALUES('Stalowe nerwy', '', 20);
INSERT INTO trump(name, description, price) VALUES('Wytrącenie z równowagi', '', 20);
INSERT INTO trump(name, description, price) VALUES('Błyskawiczny refleks', '', 20);
INSERT INTO trump(name, description, price) VALUES('Dziki talent', '', 20);
INSERT INTO trump(name, description, price) VALUES('Majątek', '', 20);
INSERT INTO trump(name, description, price) VALUES('Pierwsza pomoc', '', 20);
INSERT INTO trump(name, description, price) VALUES('Szkolenie magiczne', '', 20);


INSERT INTO trump(name, description, price) VALUES('Egzorcyzm', '', 0);
INSERT INTO trump(name, description, price) VALUES('Magiczny towarzysz', '', 0);
INSERT INTO trump(name, description, price) VALUES('Odbicie magii', '', 0);
INSERT INTO trump(name, description, price) VALUES('Przeniknięcie', '', 0);
INSERT INTO trump(name, description, price) VALUES('Przyzwanie demona', '', 0);
INSERT INTO trump(name, description, price) VALUES('Regeneracja', '', 0);
INSERT INTO trump(name, description, price) VALUES('Rozproszenie magii', '', 0);
INSERT INTO trump(name, description, price) VALUES('Wrota w Astral', '', 0);
INSERT INTO trump(name, description, price) VALUES('Wzmocnienie Atrybutu', '', 0);
INSERT INTO trump(name, description, price) VALUES('Wzmocnienie mocy (dana moc)', '', 0);
INSERT INTO trump(name, description, price) VALUES('Zawieszenie', '', 0);
INSERT INTO trump(name, description, price) VALUES('Zmiana pogody', '', 0);
INSERT INTO trump(name, description, price) VALUES('Powtórz rzut', '', 0);


-- ANALIZA (Prz)
-- BLEF (Cha)
-- EKSPRESJA (Cha)
-- EMPATIA (Prz)
-- ODWAGA (Op)
-- OKULTYZM (Prz)
-- PERSWAZJA (Cha)
-- POJAZDY (Zr)
-- PÓŁŚWIATEK (Prz)
-- PRZYRODA (Prz)
-- SPOSTRZEGAWCZOŚĆ (Prz)
-- STRZELANIE (Zr)
-- TECHNIKA (Prz)
-- UKRYWANIE SIĘ (Zr)
-- WALKA (Krz)
-- WIEDZA AKADEMICKA (Prz)
-- WYSPORTOWANIE (Krz)
-- ZASTRASZENIE (Cha)
-- ZŁODZIEJSTWO (Zr)
