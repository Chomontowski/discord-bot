START TRANSACTION;

CREATE DATABASE IF NOT EXISTS discordBot CHARACTER SET utf8 COLLATE utf8_unicode_ci;;

CREATE USER IF NOT EXISTS 'discordBot'@localhost IDENTIFIED BY 'test';

GRANT ALL PRIVILEGES ON discordBot.* TO 'discordBot'@'localhost';

COMMIT;
