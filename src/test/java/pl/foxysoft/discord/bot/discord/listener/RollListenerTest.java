package pl.foxysoft.discord.bot.discord.listener;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class RollListenerTest {

    private RollListener rollListener;

    @BeforeEach
    void prepare() {
//        rollListener = new RollListener();
    }

    @Test
    @DisplayName("Should return ten dices")
    void should_returnTenDicesDices() {
        //Given
        String message = "/wr 10";

        //When
        Integer dices = rollListener.getNumberOfDices(message);

        //Then
        assertEquals(10, dices);
    }

    @Test
    @DisplayName("Should return one dice")
    void should_returnOneDice() {
        //Given
        String message = "/wr";

        //When
        Integer dices = rollListener.getNumberOfDices(message);

        //Then
        assertEquals(1, dices);
    }

    @Test
    @DisplayName("Should return empty list")
    void should_returnEmptyList() {
        //Given
        Integer dices = 0;

        //When
        List<Integer> rolls = rollListener.makeRolls(dices);

        //Then
        assertTrue(rolls.isEmpty());
    }

    @Test
    @DisplayName("Should return the same amount of rolls as dices input")
    void should_haveSameAmountOfRollsAsDicesInput() {
        //Given
        Integer dices = 5;

        //When
        List<Integer> rolls = rollListener.makeRolls(dices);

        //Then
        assertEquals(5, rolls.size());
    }

    @Test
    @DisplayName("Should return (0 + 0 + 0 + 0) rolls")
    void should_returnZeroRollsText() {
        //Given
        List<Integer> rolls = new ArrayList<>();
        IntStream.range(0, 4).forEach(x -> rolls.add(0));

        StringBuilder exoectedMessage = new StringBuilder("Your rolls: ");
        exoectedMessage.append(System.lineSeparator());
        exoectedMessage.append("(0 + 0 + 0 + 0)");
        exoectedMessage.append(System.lineSeparator());
        exoectedMessage.append("Highest roll: 0");

        //When
        String rollsText = rollListener.createRollsText(rolls);

        //Then
        assertEquals(exoectedMessage.toString(), rollsText);

    }
}
