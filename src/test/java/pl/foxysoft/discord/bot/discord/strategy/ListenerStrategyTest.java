package pl.foxysoft.discord.bot.discord.strategy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.foxysoft.discord.bot.discord.listener.DiscordListener;
import pl.foxysoft.discord.bot.discord.listener.RollListener;
import pl.foxysoft.discord.bot.discord.listener.SessionCreationListener;
import pl.foxysoft.discord.exception.CouldNotFindListenerException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ListenerStrategyTest {

    public static final String TOTALLY_WRONG_BOT_COMMAND = "TOTALLY_WRONG_BOT_COMMAND";
    public static final String EMPTY_STRING = "";

    @InjectMocks
    private ListenerStrategy listenerStrategy;

    @Mock
    private RollListener rollListener;


    @Mock
    private SessionCreationListener sessionCreationListener;


    @BeforeEach
    void prepare() {
        when(rollListener.getShortCommand()).thenReturn("r");
        when(rollListener.getLongCommand()).thenReturn("roll");
    }

    @Test
    @DisplayName("Should get proper listener")
    void should_getProperListener() throws Exception {
        //Given
        //#prepare

        //When
        DiscordListener discordListener = listenerStrategy.getProperListener("r");

        //Then
        assertEquals("r", discordListener.getShortCommand());
        assertEquals("roll", discordListener.getLongCommand());
    }

    @Test
    @DisplayName("Should throw exception")
    void should_throwExceptionAfterNotFindingListener() {
        //Given
        //When

        //Then
        assertThrows(CouldNotFindListenerException.class, () -> {
            listenerStrategy.getProperListener(TOTALLY_WRONG_BOT_COMMAND);
        });
    }

    @Test
    @DisplayName("Should throw exception after getting empty string")
    void should_throwExceptionAfterGettingEmptyString() {
        assertThrows(CouldNotFindListenerException.class, () -> {
            listenerStrategy.getProperListener(EMPTY_STRING);
        });
    }
}
