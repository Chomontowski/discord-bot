package pl.foxysoft.discord.service;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.foxysoft.discord.persistence.model.Session;
import pl.foxysoft.discord.persistence.repository.SessionRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SessionServiceTest {

    private SessionService sessionService;

    @Mock
    private SessionRepository sessionRepository;

    @Mock
    private Member member;

    @Mock
    private Guild guild;

    private Session expectedSession;

    @BeforeEach
    void prepare() {
//        sessionService = new SessionService(sessionRepository);

        when(member.getId()).thenReturn("1");
        when(member.getGuild()).thenReturn(guild);

        when(guild.getId()).thenReturn("2");

        expectedSession.setActive(true);
        expectedSession.setOwner(1L);
//        expectedSession.setServerId(2L);

        when(sessionRepository.save(any())).thenReturn(expectedSession);
    }

    @Test
    @DisplayName("Should create session based on discord member")
    void should_createSessionBasedOnDiscordMember() {
        //Given
        //#prepare

        //When
//        Session session = sessionService.saveSession(member);

        //Then

//        assertEquals(expectedSession.getActive(), session.getActive());
//        assertEquals(expectedSession.getOwner(), session.getOwner());
//        assertEquals(expectedSession.getServerId(), session.getServerId());
    }
}
